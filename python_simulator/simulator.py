"""
- what we want to do is start from a board that has the center piece filled. since we know the algorithm will always pick the middle tile, we can save a lot of calculation by skipping ahead to it
- furthermore, we (optionally) could speed up the calculation by creating a stored hash of second turn moves based on the randomization
- to begin, i guess we can represent the board as a 25 length array of booleans. actually, scratch that, it seems like a good chance to practice bit fiddling, and it should be more efficient as bitmaps anyways
- and we may as well go further and precompute all possible winning states, and put their bitmaps into a set for extremely efficient win-checking
"""
import random
import threading
from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor
from threading import Thread

class Simulator:

    def __init__(self):
        self.winning_states = set()

    # precompute all possible winning states, and store each as a bitmap into a set

    def precomputeOutcomesForState(self, state, tile, turns_used):
        global winning_states
        if tile >= 26:
            return

        if turns_used >= 16:
            if self.isWinningState(state):
                self.winning_states.add(state)
            return

        if (state >> tile) & 1:
            self.precomputeOutcomesForState(state, tile + 1, turns_used)
        else:
            mask = 1 << tile
            state |= mask
            self.precomputeOutcomesForState(state, tile + 1, turns_used + 1)
            inverted_mask = ~mask
            state &= inverted_mask
            self.precomputeOutcomesForState(state, tile + 1, turns_used)

    def isWinningState(self, state):
        bingos_won = 0
        #Diagonals
        if ((state >> 0) & 1) and ((state >> 6) & 1) and ((state >> 12) & 1) and ((state >> 18) & 1) and ((state >> 24) & 1):
            bingos_won += 1

        if ((state >> 4) & 1) and ((state >> 8) & 1) and ((state >> 12) & 1) and ((state >> 16) & 1) and ((state >> 20) & 1):
            bingos_won += 1

        #Horizontals
        if ((state >> 0) & 1) and ((state >> 1) & 1) and ((state >> 2) & 1) and ((state >> 3) & 1) and ((state >> 4) & 1):
            bingos_won += 1

        if ((state >> 5) & 1) and ((state >> 6) & 1) and ((state >> 7) & 1) and ((state >> 8) & 1) and ((state >> 9) & 1):
            bingos_won += 1

        if ((state >> 10) & 1) and ((state >> 11) & 1) and ((state >> 12) & 1) and ((state >> 13) & 1) and ((state >> 14) & 1):
            bingos_won += 1

        if ((state >> 15) & 1) and ((state >> 16) & 1) and ((state >> 17) & 1) and ((state >> 18) & 1) and ((state >> 19) & 1):
            bingos_won += 1

        if ((state >> 20) & 1) and ((state >> 21) & 1) and ((state >> 22) & 1) and ((state >> 23) & 1) and ((state >> 24) & 1):
            bingos_won += 1

        #Verticals
        if ((state >> 0) & 1) and ((state >> 5) & 1) and ((state >> 10) & 1) and ((state >> 15) & 1) and ((state >> 20) & 1):
            bingos_won += 1

        if ((state >> 1) & 1) and ((state >> 6) & 1) and ((state >> 11) & 1) and ((state >> 16) & 1) and ((state >> 21) & 1):
            bingos_won += 1

        if ((state >> 2) & 1) and ((state >> 7) & 1) and ((state >> 12) & 1) and ((state >> 17) & 1) and ((state >> 22) & 1):
            bingos_won += 1

        if ((state >> 3) & 1) and ((state >> 8) & 1) and ((state >> 13) & 1) and ((state >> 18) & 1) and ((state >> 23) & 1):
            bingos_won += 1

        if ((state >> 4) & 1) and ((state >> 9) & 1) and ((state >> 14) & 1) and ((state >> 19) & 1) and ((state >> 24) & 1):
            bingos_won += 1

        if bingos_won >= 4:
            return True

        return False

    # implement the recursive tile-counting algorithm from the javascript web tool here in Python
    # it should accept a state and return the optimal next move (as a modified state)
    # or if it has multiple equally well-recommended moves, return one of them at random
    def tileCountingAlgorithmNextMove(self, state, turns_used):
        win_counts = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

        def countWinsForState(state, tile, turns_used):
            if tile >= 26:
                return

            if turns_used >= 16:
                if state in self.winning_states:
                    for i in range(25):
                        if (state >> i) & 1:
                            win_counts[i] += 1

            if (state >> tile) & 1:
                countWinsForState(state, tile + 1, turns_used)
            else:
                mask = 1 << tile
                state |= mask
                countWinsForState(state, tile + 1, turns_used + 1)
                inverted_mask = ~mask
                state &= inverted_mask
                countWinsForState(state, tile + 1, turns_used)

        countWinsForState(state, 0, turns_used)
        # an important step is to remove win counts for tiles that are already selected
        for i in range(25):
            if (state >> i) & 1:
                win_counts[i] = 0
        #print("Finished calculating win counts for player turn. They look like:", win_counts)
        highest = max(win_counts)
        highest_count = win_counts.count(highest)
        #print("Number of highest among win_counts list:", highest_count)
        if highest_count == 1:
            highest_index = win_counts.index(highest)
            mask = 1 << highest_index
            state |= mask
        else:
            # there are multiple best choices
            choice_indices = []
            for i in range(25):
                if win_counts[i] == highest:
                    choice_indices.append(i)
            mask = 1 << random.choice(choice_indices)
            state |= mask

        return state

    # accepts a state and makes a single random move out of the available open positions, then returns that new state
    def habbyNextMove(self, state):
        move_options = []
        for i in range(25):
            if ((state >> i) & 1) == 0:
                move_options.append(i)
        mask = 1 << random.choice(move_options)
        state |= mask
        return state

    # designed to be run in a thread, runs a game and returns true or false for whether it was a win
    def simulateGame(self):
        state = 1 << 12
        turns_used = 1
        while turns_used < 16:
            #print("Turn #", turns_used + 1)
            # even number of turns used is player's turn
            if turns_used % 2 == 0:
                state = self.tileCountingAlgorithmNextMove(state, turns_used)
                #print("State after player turn:", bin(state))
            # odd number of turns used is Habby's turn
            else:
                state = self.habbyNextMove(state)
                #print("State after Habby turn: ", bin(state))
            turns_used += 1

        if state in self.winning_states:
            return True
        return False



# a thread pool that can efficiently run many games in parallel
def main():
    games_simulated = 0
    desired_sample_size = 1000
    games_won = 0

    simulator = Simulator()

    def handleFinishedGame(future):
        nonlocal games_won
        nonlocal games_simulated
        if future.result() == True:
            games_won += 1
        games_simulated += 1
        print("Simulated game #", games_simulated, ", and have won", games_won, "of them so far.")

    # set initial state to have tile 12, the middle tile, pre-selected, since this is always the algorithm's first choice
    initial_state = 1 << 12
    print("Constructed initial state:", bin(initial_state), "and will now begin precomputation of winning outcomes")
    # note that this does not include every possible winning outcome, but rather only those that include the middle tile. For the purposes of this simulation, it's enough
    simulator.precomputeOutcomesForState(initial_state, 0, 1)
    print("Finished computing all winning end states. Found", len(simulator.winning_states), " winning outcomes.")

    games_started = 0

    with ProcessPoolExecutor(max_workers=12) as executor:
        while games_started < desired_sample_size:
            # simulate games
            future = executor.submit(simulator.simulateGame)
            future.add_done_callback(handleFinishedGame)
            games_started += 1
            print("Started game #", games_started)

    print("Simulation finished. We've reached the desired sample size of", desired_sample_size)


if __name__ == '__main__':
    main()

