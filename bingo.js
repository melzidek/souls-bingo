var NUM_TURNS = 16;
var NUM_TARGET_BINGOS = 4;
var turns_remaining = 16;
var possibilities_calculated = 0;
var wins_calculated = 0;
var win_counts = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
var currently_selected_tiles;

$('#bingo td').on("click", function() {
    $(this).toggleClass("on");
    handleTileClick();
});

$('#reset').on("click", function() {
    location.reload();
});

function handleTileClick() {
    possibilities_calculated = 0;
    wins_calculated = 0;
    win_counts = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    turns_remaining = NUM_TURNS - $('#bingo td.on').length
    currently_selected_tiles = captureBoardState();

    calculatePossibilitiesForState(captureBoardState(), 0, 0);
    $('#analysis').prepend("<li>Calculated " + possibilities_calculated + " possibilities, and found " + wins_calculated + " wins. Number of turns remaining is " + turns_remaining + "</li>");
    distributeWinCounts();
    updatePanel();
}

function captureBoardState() {
    var out = [];
    $('#bingo td').each(function() {
        out.push($(this).hasClass('on'));
    });
    console.log('captureBoardState called, result is: ' + out);
    return out;
}

function isWinningState(state) {
    var bingos_won = 0;
    //Diagonals
    if(state[0] && state[6] && state[12] && state[18] && state[24]) {
        bingos_won++;
    }
    if(state[4] && state[8] && state[12] && state[16] && state[20]) {
        bingos_won++;
    }
    //Horizontals
    if(state[0] && state[1] && state[2] && state[3] && state[4]) {
        bingos_won++;
    }
    if(state[5] && state[6] && state[7] && state[8] && state[9]) {
        bingos_won++;
    }
    if(state[10] && state[11] && state[12] && state[13] && state[14]) {
        bingos_won++;
    }
    if(state[15] && state[16] && state[17] && state[18] && state[19]) {
        bingos_won++;
    }
    if(state[20] && state[21] && state[22] && state[23] && state[24]) {
        bingos_won++;
    }
    //Verticals
    if(state[0] && state[5] && state[10] && state[15] && state[20]) {
        bingos_won++;
    }
    if(state[1] && state[6] && state[11] && state[16] && state[21]) {
        bingos_won++;
    }
    if(state[2] && state[7] && state[12] && state[17] && state[22]) {
        bingos_won++;
    }
    if(state[3] && state[8] && state[13] && state[18] && state[23]) {
        bingos_won++;
    }
    if(state[4] && state[9] && state[14] && state[19] && state[24]) {
        bingos_won++;
    }
    if (bingos_won >= NUM_TARGET_BINGOS) {
        return true;
    }
    return false;
}

function calculatePossibilitiesForState(state, tile, turns_used) {
    if(tile >= 26) {
        return;
    }
    //console.log("possibilities_calculated: ", possibilities_calculated, "i_start", i_start, "turns_used", turns_used);
    if(turns_used >= turns_remaining) {
        possibilities_calculated++;
        //console.log("possibilities calculated: " + possibilities_calculated);
        if(isWinningState(state)) {
            wins_calculated++;
            //console.log("found a winner! Looks like: " + state);
            for(var i=0; i<25; i++) {
                if(state[i]) {
                    win_counts[i]++;
                }
            }
        }
        return;
    }

    if(currently_selected_tiles[tile] == true) {
        calculatePossibilitiesForState(state, tile+1, turns_used);
    } else {
        state[tile] = true;
        calculatePossibilitiesForState(state, tile+1, turns_used+1);
        state[tile] = false;
        calculatePossibilitiesForState(state, tile+1, turns_used);
    }
}

function distributeWinCounts() {
    $('#bingo td.best-choice').removeClass("best-choice");
    $('#bingo td').empty();
    for(var i=0; i<25; i++) {
        if(currently_selected_tiles[i]) {
            win_counts[i] = 0;
        }
    }
    const max = win_counts.reduce((a, b) => Math.max(a, b), -Infinity);

    $('#bingo td').each(function(index, element) {
        if(currently_selected_tiles[index]) {
            return;
        }
        win_percentage = win_counts[index] / wins_calculated;
        win_percentage = Math.round(win_percentage*10000) / 100
        $(element).text(win_percentage + "%");
        if(max == win_counts[index]) {
            $(element).addClass("best-choice");
        }
    });
}

function updatePanel() {
    if(turns_remaining % 2 == 0) {
        $('#status > .slot').text("Player's Turn");
        $('body').addClass('player');
        $('body').removeClass('habby');
    } else {
        $('#status > .slot').text("Habby's Turn");
        $('body').addClass('habby');
        $('body').removeClass('player');
    }
    $('#habby-turns > .slot').text(Math.round(turns_remaining / 2))
    $('#player-turns > .slot').text(Math.round((turns_remaining - 1) / 2))
}

handleTileClick();


//assign indices to cells for visual convenience
/*
bingo_td_index = 0;
$('#bingo td').each(function() {
    $(this).text(bingo_td_index++);
})
*/
