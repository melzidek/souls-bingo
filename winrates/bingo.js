var NUM_TURNS = 16;
var NUM_TARGET_BINGOS = 4;
var turns_remaining = 16;
var possibilities_calculated = 0;
var wins_calculated = 0;
var win_counts = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
var win_rates = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
var outcomes = BigInt(0)
var currently_selected_tiles;

$('#bingo td').on("click", function() {
    $(this).toggleClass("on");
    handleTileClick();
});

$('#reset').on("click", function() {
    location.reload();
});


function getOutcomesIndexFromState(state) {
    index = 0
    for(var i=0; i<25; i++) {
        if(state[i]) {
            index += 2 ** i
        }
    }
    return index
}

// this function should iterate through every possible endgame board state, and mark it as a win or loss
// storing each result as a true or false in a bitmask, since JS stores booleans as bytes and we have too many
// to retrieve the value of a board state, we can concatenate all of the tiles states into a binary string and take that as the index
function precomputeOutcomesForState(state, tile, turns_used) {
    if(tile >= 26) {
        return;
    }
    if(turns_used >= turns_remaining) {
        possibilities_calculated++;
        if(isWinningState(state)) {
            offset = getOutcomesIndexFromState(state)
            mask = BigInt(1) << BigInt(offset)
            outcomes |= BigInt(mask)
            console.log("found a winning state. offset is:" + offset + ", mask is:" + offset + ", and outcomes is now:" + outcomes.toString(2))
            wins_calculated++;
        }
        return;
    }

    if(state[tile] == true) {
        precomputeOutcomesForState(state, tile+1, turns_used);
    } else {
        state[tile] = true;
        precomputeOutcomesForState(state, tile+1, turns_used+1);
        state[tile] = false;
        precomputeOutcomesForState(state, tile+1, turns_used);
    }
}

function handleTileClick() {
    possibilities_calculated = 0;
    wins_calculated = 0;
    win_counts = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    win_rates = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    turns_remaining = NUM_TURNS - $('#bingo td.on').length
    currently_selected_tiles = captureBoardState();

    //calculatePossibilitiesForState(captureBoardState(), 0, 0);
    calculateWinRatesForBoard(captureBoardState());
    $('#analysis').prepend("<li>Calculated " + possibilities_calculated + " possibilities, and found " + wins_calculated + " wins. Number of turns remaining is " + turns_remaining + "</li>");
    distributeWinRates();
    //distributeWinCounts();
    updatePanel();
}

function captureBoardState() {
    var out = [];
    $('#bingo td').each(function() {
        out.push($(this).hasClass('on'));
    });
    console.log('captureBoardState called, result is: ' + out);
    return out;
}

function isWinningState(state) {
    var bingos_won = 0;
    //Diagonals
    if(state[0] && state[6] && state[12] && state[18] && state[24]) {
        bingos_won++;
    }
    if(state[4] && state[8] && state[12] && state[16] && state[20]) {
        bingos_won++;
    }
    //Horizontals
    if(state[0] && state[1] && state[2] && state[3] && state[4]) {
        bingos_won++;
    }
    if(state[5] && state[6] && state[7] && state[8] && state[9]) {
        bingos_won++;
    }
    if(state[10] && state[11] && state[12] && state[13] && state[14]) {
        bingos_won++;
    }
    if(state[15] && state[16] && state[17] && state[18] && state[19]) {
        bingos_won++;
    }
    if(state[20] && state[21] && state[22] && state[23] && state[24]) {
        bingos_won++;
    }
    //Verticals
    if(state[0] && state[5] && state[10] && state[15] && state[20]) {
        bingos_won++;
    }
    if(state[1] && state[6] && state[11] && state[16] && state[21]) {
        bingos_won++;
    }
    if(state[2] && state[7] && state[12] && state[17] && state[22]) {
        bingos_won++;
    }
    if(state[3] && state[8] && state[13] && state[18] && state[23]) {
        bingos_won++;
    }
    if(state[4] && state[9] && state[14] && state[19] && state[24]) {
        bingos_won++;
    }
    if (bingos_won >= NUM_TARGET_BINGOS) {
        return true;
    }
    return false;
}

function calculateWinRateForState(state, turns_used) {
    possibilities_calculated++;
    //base case, reached the end of the game
    if(turns_used >= turns_remaining) {
        if(isWinningState(state)) {
            wins_calculated++;
            return 1.0
        }
        return 0.0
    }

    //otherwise, get the average of win rates of all remaining open tiles, as if they were toggled
    var win_rates_sum = 0.0
    var win_rates_count = 0.0
    var max_win_rate = 0.0
    for(var i=0; i<25; i++) {
        if(!state[i]) {
            state[i] = true
            win_rate = calculateWinRateForState(state, turns_used + 1)
            //console.log("got back a win_rate from next recursion layer of:" + win_rate)
            win_rates_sum += win_rate
            win_rates_count++;
            max_win_rate = Math.max(max_win_rate, win_rate)
            state[i] = false
        }
    }
    if( (turns_remaining - turns_used) % 2 == 0 ) {
        // player turn, take the max
        return max_win_rate
    }
    //console.log("turns used:" + turns_used + ", win_rates_sum:" + win_rates_sum + ", win_rates_count:" + win_rates_count)
    return win_rates_sum / win_rates_count
}

function calculateWinRatesForBoard(state) {
    for(var i=0; i<25; i++) {
        if(!state[i]) {
            state[i] = true
            win_rates[i] = calculateWinRateForState(state, 1)
            state[i] = false
        }
    }
}

function calculatePossibilitiesForState(state, tile, turns_used) {
    if(tile >= 26) {
        return;
    }
    //console.log("possibilities_calculated: ", possibilities_calculated, "i_start", i_start, "turns_used", turns_used);
    if(turns_used >= turns_remaining) {
        possibilities_calculated++;
        //console.log("possibilities calculated: " + possibilities_calculated);
        if(isWinningState(state)) {
            wins_calculated++;
            //console.log("found a winner! Looks like: " + state);
            for(var i=0; i<25; i++) {
                if(state[i]) {
                    win_counts[i]++;
                }
            }
        }
        return;
    }

    if(currently_selected_tiles[tile] == true) {
        calculatePossibilitiesForState(state, tile+1, turns_used);
    } else {
        state[tile] = true;
        calculatePossibilitiesForState(state, tile+1, turns_used+1);
        state[tile] = false;
        calculatePossibilitiesForState(state, tile+1, turns_used);
    }
}

function distributeWinRates() {
    console.log("Distributing win rates:" + win_rates)
    $('#bingo td.best-choice').removeClass("best-choice");
    $('#bingo td').empty();

    const max = win_rates.reduce((a, b) => Math.max(a, b), -Infinity);

    $('#bingo td').each(function(index, element) {
        if(currently_selected_tiles[index]) {
            return;
        }
        display_percentage = Math.round(win_rates[index]*10000) / 100
        $(element).text(display_percentage + "%");
        if(max == win_rates[index]) {
            $(element).addClass("best-choice");
        }
    });
}

function distributeWinCounts() {
    $('#bingo td.best-choice').removeClass("best-choice");
    $('#bingo td').empty();
    for(var i=0; i<25; i++) {
        if(currently_selected_tiles[i]) {
            win_counts[i] = 0;
        }
    }
    const max = win_counts.reduce((a, b) => Math.max(a, b), -Infinity);

    $('#bingo td').each(function(index, element) {
        if(currently_selected_tiles[index]) {
            return;
        }
        win_percentage = win_counts[index] / wins_calculated;
        win_percentage = Math.round(win_percentage*10000) / 100
        $(element).text(win_percentage + "%");
        if(max == win_counts[index]) {
            $(element).addClass("best-choice");
        }
    });
}

function updatePanel() {
    if(turns_remaining % 2 == 0) {
        $('#status > .slot').text("Player's Turn");
        $('body').addClass('player');
        $('body').removeClass('habby');
    } else {
        $('#status > .slot').text("Habby's Turn");
        $('body').addClass('habby');
        $('body').removeClass('player');
    }
    $('#habby-turns > .slot').text(Math.round(turns_remaining / 2))
    $('#player-turns > .slot').text(Math.round((turns_remaining - 1) / 2))
}

//currently_selected_tiles = captureBoardState();
//precomputeOutcomesForState(captureBoardState(), 0, 0);
//console.log("Finished pre-computing all possible game outcomes. Stored in a BigInt mask as follows:" + outcomes)
handleTileClick();


//assign indices to cells for visual convenience
/*
bingo_td_index = 0;
$('#bingo td').each(function() {
    $(this).text(bingo_td_index++);
})
*/
